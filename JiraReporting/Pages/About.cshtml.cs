﻿using System.Linq;
using JiraReporting.BL;
using JiraReporting.BL.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace JiraReporting.Pages
{
	public class AboutModel : PageModel
	{
		public AboutModel(IIssueService issueService)
		{
			_issueService = issueService;
		}

		private readonly IIssueService _issueService;
		public string Message { get; set; }	

		public void OnGet()
		{
			Message = "Your application description page.";

			var issues = _issueService.GetIssues();

			Message = issues.First().Summary;
		}
	}
}
