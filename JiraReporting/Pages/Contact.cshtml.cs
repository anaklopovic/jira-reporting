﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JiraReporting.BL.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace JiraReporting.Pages
{
	public class ContactModel : PageModel
	{
		public ContactModel(IWorklogService worklogService)
		{
			_worklogService = worklogService;
		}

		private readonly IWorklogService _worklogService;

		public string Message { get; set; }

		public void OnGet()
		{
			Message = "Your contact page.";

			_worklogService.FetchWorklogs();
		}
	}
}
