﻿using System.Collections.Generic;

namespace JiraReporting.BL.Services
{
	public interface IIssueService
	{
		List<IssueDTO> GetIssues();
	}
}
