﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JiraReporting.APIClient;
using JiraReporting.APIClient.Responses;
using JiraReporting.DAL.Models;
using JiraReporting.DAL.Repositories;

namespace JiraReporting.BL.Services
{
	public class WorklogService : IWorklogService
	{
		private readonly IJiraAPIClient _jiraAPIClient;
		private readonly IDBRepository _dBRepository;

		public WorklogService(
			IJiraAPIClient jiraAPIClient,
			IDBRepository dBRepository)
		{
			_jiraAPIClient = jiraAPIClient;
			_dBRepository = dBRepository;
		}

		public List<int> GetUpdatedWorklogIDs(DateTime lastUpdated)
		{
			throw new NotImplementedException();
		}


		public void FetchWorklogs()
		{
			var worklogIDs = _jiraAPIClient.GetUpdatedWorklogIDs(new DateTime(2020, 9,1));

			var worklogResponses = _jiraAPIClient.GetWorklogsPaginated(worklogIDs);
			var worklogs = MapResponseToWorklogs(worklogResponses);
			_dBRepository.Worklogs.InsertOrUpdate(worklogs);
		}

		private List<Worklog> MapResponseToWorklogs(IEnumerable<WorklogResponse> worklogResponses)
		{
			var result = worklogResponses.Select(x => new Worklog
			{
				WorklogID = x.ID,
				IssueID = x.IssueID,
				Author = x.Author.DisplayName,
				Started = x.Started,
				Updated = x.Updated,
				TimeSpentSeconds = x.TimeSpentSeconds
			}).ToList();

			return result;
		}
	}
}
