﻿using System.Collections.Generic;
using System.Linq;
using JiraReporting.APIClient;
using JiraReporting.APIClient.Responses;
using JiraReporting.DAL.Models;
using JiraReporting.DAL.Repositories;

namespace JiraReporting.BL.Services
{
	public class IssueService : IIssueService
	{
		private readonly IJiraAPIClient _jiraAPIClient;
		private readonly IDBRepository _dBRepository;

		public IssueService (
			IJiraAPIClient jiraAPIClient,
			IDBRepository dBRepository)
		{
			_jiraAPIClient = jiraAPIClient;
			_dBRepository = dBRepository;
		}

		public List<IssueDTO> GetIssues()
		{
			var result = new List<IssueDTO>
			{
				new IssueDTO
				{
					IssueID = 3,
					Summary = "Test 3"
				}
			};


			var jql = "";
			var fields = new List<JiraFieldsEnum>
			{
				JiraFieldsEnum.Parent,
				JiraFieldsEnum.Timeestimate,
				JiraFieldsEnum.Summary
			};

			var issueResponse = _jiraAPIClient.SearchIssues(jql, fields);
			var issues = MapIssueResponseToModel(issueResponse);
			_dBRepository.Issues.InsertOrUpdate(issues);

			return result;
		}

		private List<Issue> MapIssueResponseToModel (IEnumerable<IssueResponse> issueDTOs)
		{
			var result = issueDTOs.Select(x =>
			{
				var issue = new Issue
				{
					IssueID = x.ID
				};

				if (x.Fields != null)
				{
					if (!string.IsNullOrWhiteSpace(x.Fields.Summary))
					{
						issue.Summary = x.Fields.Summary;
					}
				}

				return issue;
			}).ToList();

			return result;
		}
	}
}
