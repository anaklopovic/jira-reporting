﻿namespace JiraReporting.BL
{
	public class IssueDTO
	{
		public int IssueID { get; set; }
		public string Summary { get; set; }
	}
}
