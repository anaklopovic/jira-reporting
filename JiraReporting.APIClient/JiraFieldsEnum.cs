﻿namespace JiraReporting.APIClient
{
	public enum JiraFieldsEnum
	{
		Parent,
		Timeestimate,
		Timetracking,
		Summary
	}
}
