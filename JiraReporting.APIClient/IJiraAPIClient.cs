﻿using System;
using System.Collections.Generic;
using JiraReporting.APIClient.Responses;

namespace JiraReporting.APIClient
{
	public interface IJiraAPIClient
	{
		List<IssueResponse> SearchIssues(string jql, ICollection<JiraFieldsEnum> fields = null);

		List<WorklogResponse> GetWorklogs(List<int> worklogIDs);

		List<int> GetUpdatedWorklogIDs(DateTime since);

		List<WorklogResponse> GetWorklogsPaginated(List<int> worklogIDs);
	}
}
