﻿namespace JiraReporting.APIClient
{
	public enum JiraResourceEnum
	{
		Issue,
		Search,
		Worklog
	}
}
