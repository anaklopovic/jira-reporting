﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using JiraReporting.APIClient.Responses;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;

namespace JiraReporting.APIClient
{
	public class JiraAPIClient : IJiraAPIClient
	{
		static HttpClient client = new HttpClient();
		private const string BaseUrl = "https://lemaxnet.atlassian.net/rest/api/3/";
		private const string Username = "ana.klopovic@lemax.net";
		private const string Password = "xewVEYxq3viBTW3A3dDB9365";
		private const int MAX_RESULTS = 100;
		private const int MAX_WORKLOG_FETCH = 1000;

		public List<IssueResponse> SearchIssues(string jql, ICollection<JiraFieldsEnum> fields = null)
		{
			var queryParameters = new Dictionary<string, string>();
			queryParameters.Add("jql", jql);

			if (fields != null && fields.Count > 0)
			{
				var fieldsString = string.Join(',', fields.Select(x => x.ToString().ToLower()));
				queryParameters.Add("fields", fieldsString);
			}

			var startAt = 0;

			queryParameters.Add("maxResults", MAX_RESULTS.ToString());
			queryParameters.Add("startAt", startAt.ToString());

			var resultIssues = new List<IssueResponse>();

			var remainingIssues = 0;

			do
			{
				var response = GetRestResponse(JiraResourceEnum.Search, queryParameters: queryParameters);
				var deserializer = new JsonDeserializer();
				var searchResponse = deserializer.Deserialize<SearchResponse>(response);
				resultIssues.AddRange(searchResponse.Issues);
				startAt = startAt + MAX_RESULTS;
				queryParameters["startAt"] = startAt.ToString();
				remainingIssues = searchResponse.Total - startAt;
			} while (remainingIssues > 0);

			return resultIssues;
		}

		public List<int> GetUpdatedWorklogIDs(DateTime since)
		{
			var sinceEpoch = new DateTimeOffset(since, TimeSpan.Zero).ToUnixTimeMilliseconds();
			var queryParameters = new Dictionary<string, string>();
			queryParameters.Add("since", sinceEpoch.ToString());

			var resultWorklogIDs = new List<int>();
			var lastPage = false;

			do
			{
				var response = GetRestResponse(JiraResourceEnum.Worklog, argument: "updated", queryParameters: queryParameters);
				var deserializer = new JsonDeserializer();
				var updatedWorklogResponses = deserializer.Deserialize<UpdatedWorklogsResponse>(response);
				resultWorklogIDs.AddRange(updatedWorklogResponses.Values.Select(x => x.WorklogID));
				lastPage = updatedWorklogResponses.LastPage;
				queryParameters["since"] = updatedWorklogResponses.Until.ToString();
			} while (!lastPage);

			var result = resultWorklogIDs
				.Distinct()
				.ToList();
			return result;
		}

		public List<WorklogResponse> GetWorklogsPaginated(List<int> worklogIDs)
		{
			var bla = worklogIDs.Where(x => x == 30019);

			var pageNumber = 1;
			var pageSize = MAX_RESULTS;
			var resultWorklogs = new List<WorklogResponse>();

			var skip = 0;
			while (true)
			{
				var pageIDs = worklogIDs
				.Skip(skip)
				.Take(pageSize)
				.ToList();

				if (pageIDs.Count == 0)
				{
					break;
				}

				var pageResult = GetWorklogs(pageIDs);
				resultWorklogs.AddRange(pageResult);

				skip = pageNumber * pageSize;
				pageNumber++;
			}
			
			return resultWorklogs;
		}

		public List<WorklogResponse> GetWorklogs(List<int> worklogIDs)
		{
			var body = new
			{
				ids = worklogIDs
			};

			var remainingWorklogs = worklogIDs.Count;
			var resultWorklogs = new List<WorklogResponse>();

			do
			{
				var response = GetRestResponse(JiraResourceEnum.Worklog, argument: "list", body: body, method: Method.POST);
				var deserializer = new JsonDeserializer();
				var worklogResponses = deserializer.Deserialize<List<WorklogResponse>>(response);
				resultWorklogs.AddRange(worklogResponses);
				remainingWorklogs -= MAX_RESULTS;
			} while (remainingWorklogs > 0);

			return resultWorklogs;
		}

		private IRestResponse GetRestResponse(JiraResourceEnum resource, string argument = null, Dictionary<string, string> queryParameters = null, object body = null, Method method = Method.GET)
		{
			var url = string.Format("{0}{1}", BaseUrl, resource.ToString().ToLower());
			if (argument != null)
			{
				url = string.Format("{0}/{1}", url, argument);
			}
			
			if (queryParameters != null)
			{
				url = string.Format("{0}?", url);
				foreach (var queryParameter in queryParameters)
				{
					url = string.Format("{0}{1}={2}&", url, queryParameter.Key, queryParameter.Value);
				}
				url = url.Substring(0, url.Length - 1);
			}

			var client = new RestClient(url);
			client.Authenticator = new HttpBasicAuthenticator(Username, Password);
			client.Timeout = -1;			
			
			var request = new RestRequest(method);

			if (body != null)
			{
				request.AddJsonBody(body);
			}

			IRestResponse response = client.Execute(request);
			return response;
		}
	}
}
