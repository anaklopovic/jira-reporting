﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JiraReporting.APIClient.Responses
{
	public class AuthorResponse
	{
		public string AccountID { get; set; }
		public string DisplayName { get; set; }
	}
}
