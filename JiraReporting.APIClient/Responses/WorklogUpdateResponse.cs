﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JiraReporting.APIClient.Responses
{
	public class WorklogUpdateResponse
	{
		public int WorklogID { get; set; }
	}
}
