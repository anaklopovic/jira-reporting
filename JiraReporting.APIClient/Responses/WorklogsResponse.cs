﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JiraReporting.APIClient.Responses
{
	public class WorklogsResponse
	{
		public List<WorklogResponse> Worklogs { get; set; }
	}
}
