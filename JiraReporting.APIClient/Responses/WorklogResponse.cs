﻿using System;

namespace JiraReporting.APIClient.Responses
{
	public class WorklogResponse
	{
		public int ID { get; set; }
		public int IssueID { get; set; }

		public int TimeSpentSeconds { get; set; }
		public DateTime Started { get; set; }
		public DateTime Updated { get; set; }
		public AuthorResponse Author { get; set; }
		public AuthorResponse UpdateAuthor { get; set; }
	}
}
