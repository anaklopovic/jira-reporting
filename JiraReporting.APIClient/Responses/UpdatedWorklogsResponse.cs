﻿using System.Collections.Generic;

namespace JiraReporting.APIClient.Responses
{
	public class UpdatedWorklogsResponse
	{
		public long Until { get; set; }
		public bool LastPage { get; set; }

		public List<WorklogUpdateResponse> Values { get; set; }
	}
}
