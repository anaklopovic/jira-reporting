﻿namespace JiraReporting.APIClient.Responses
{
	public class FieldsResponse
	{
		public int TimeEstimate { get; set; }
		public IssueResponse Parent { get; set; }
		public string Summary { get; set; }
	}
}
