﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JiraReporting.APIClient.Responses
{
	public class SearchResponse
	{
		public int StartAt { get; set; }
		public int MaxResults { get; set; }
		public int Total { get; set; }

		public List<IssueResponse> Issues { get; set; }
	}
}
