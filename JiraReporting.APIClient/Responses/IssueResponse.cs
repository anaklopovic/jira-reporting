﻿using System.Collections.Generic;

namespace JiraReporting.APIClient.Responses
{
	public class IssueResponse
	{
		public int ID { get; set; }
		public string Key { get; set; }

		public FieldsResponse Fields { get; set; }
	}
}
