﻿namespace JiraReporting.DAL.Repositories
{
	public class DBRepository : IDBRepository
	{		
		public DBRepository(
			IIssueRepository issueRepository,
			IWorklogRepository worklogRepository)
		{
			Issues = issueRepository;
			Worklogs = worklogRepository;
		}

		public IIssueRepository Issues { get; }
		public IWorklogRepository Worklogs { get; }
	}
}
