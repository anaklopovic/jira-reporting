﻿using System.Collections.Generic;

namespace JiraReporting.DAL.Repositories
{
	public interface IGenericRepository<TModel> where TModel : class
	{
		void InsertOrUpdate(ICollection<TModel> entitiesToInsert);
	}
}
