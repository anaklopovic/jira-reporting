﻿using JiraReporting.DAL.Models;

namespace JiraReporting.DAL.Repositories
{
	public interface IIssueRepository : IGenericRepository<Issue>
	{
	}
}
