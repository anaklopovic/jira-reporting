﻿namespace JiraReporting.DAL.Repositories
{
	public interface IDBRepository
	{
		IIssueRepository Issues { get; }
		IWorklogRepository Worklogs { get; }
	}
}
