﻿using JiraReporting.DAL.Models;

namespace JiraReporting.DAL.Repositories
{
	public class IssueRepository : GenericRepository<Issue>, IIssueRepository
	{
		public IssueRepository(ApplicationDbContext context)
		{
			base.context = context;
		}

		/// <summary>
		/// Name of the identity (key) property
		/// </summary>
		protected override string IdentityKeyPropertyName => "IssueID";

		protected override string DBSetName => "Issues";

		/// <summary>
		/// Method that updates the properties of the object
		/// </summary>
		/// <param name="entityToUpdate">Entity that will get the update values</param>
		/// <param name="updatedEntity">Entity from which values are read</param>
		protected override void UpdateProperties(Issue entityToUpdate, Issue updatedEntity)
		{
			entityToUpdate.Summary = updatedEntity.Summary;
		}
	}
}
