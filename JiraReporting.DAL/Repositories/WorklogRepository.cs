﻿using JiraReporting.DAL.Models;

namespace JiraReporting.DAL.Repositories
{
	public class WorklogRepository : GenericRepository<Worklog>, IWorklogRepository
	{
		public WorklogRepository(ApplicationDbContext context)
		{
			base.context = context;
		}

		protected override string IdentityKeyPropertyName => "WorklogID";

		protected override string DBSetName => "Worklogs";

		protected override void UpdateProperties(Worklog entityToUpdate, Worklog updatedEntity)
		{
			entityToUpdate.Author = updatedEntity.Author;
		}
	}
}
