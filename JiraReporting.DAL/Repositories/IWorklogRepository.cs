﻿using JiraReporting.DAL.Models;

namespace JiraReporting.DAL.Repositories
{
	public interface IWorklogRepository : IGenericRepository<Worklog> 
	{
	}
}
