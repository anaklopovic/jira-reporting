﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace JiraReporting.DAL.Repositories
{
	public abstract class GenericRepository<TModel> : IGenericRepository<TModel> where TModel : class
	{
		protected ApplicationDbContext context;
		protected abstract string IdentityKeyPropertyName { get;}
		protected abstract string DBSetName { get;}

		/// <summary>
		/// Method that updates the properties of the object
		/// </summary>
		/// <param name="entityToUpdate">Entity that will get the update values</param>
		/// <param name="updatedEntity">Entity from which values are read</param>
		protected abstract void UpdateProperties(TModel entityToUpdate, TModel updatedEntity);

		/// <summary>
		/// Inserts new entities or updates them if they already exist.
		/// Entity comparison is done by KeyPropertyName
		/// </summary>
		/// <param name="entities"></param>
		public void InsertOrUpdate(ICollection<TModel> entities)
		{
			context.Database.BeginTransaction();
			try
			{
				var dbSet = context.Set<TModel>();
				var keyProperty = typeof(TModel).GetProperty(IdentityKeyPropertyName);

				var entitiesByID = entities
				.ToDictionary(x => keyProperty.GetValue(x), x => x);

				var existingEntities = dbSet
					.Where(x => entitiesByID.Keys.Contains(keyProperty.GetValue(x)))
					.ToDictionary(x => keyProperty.GetValue(x, null), x => x);

				var newEntities = entities
					.Where(x => !existingEntities.Keys.Contains(keyProperty.GetValue(x)))
					.ToList();

				foreach (var existingIssue in existingEntities.Values)
				{
					var updatedIssue = entitiesByID[keyProperty.GetValue(existingIssue)];
					UpdateProperties(existingIssue, updatedIssue);
					dbSet.Update(existingIssue);
				}

				dbSet.AddRange(newEntities);
				var sqlCommand = string.Format("SET IDENTITY_INSERT dbo.{0} ON", DBSetName);
				context.Database.ExecuteSqlCommand(sqlCommand);
				context.SaveChanges();
				sqlCommand = string.Format("SET IDENTITY_INSERT dbo.{0} OFF", DBSetName);
				context.Database.ExecuteSqlCommand(sqlCommand);
				context.Database.CommitTransaction();
			}
			catch (Exception e)
			{
				context.Database.RollbackTransaction();
				throw e;
			}	
		}
	}
}
