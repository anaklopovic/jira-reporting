﻿using JiraReporting.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace JiraReporting.DAL
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
			Database.EnsureCreated();
		}

		public virtual DbSet<Issue> Issues { get; set; }
		public virtual DbSet<Worklog> Worklogs { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
