﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JiraReporting.DAL.Models
{
	public class Issue
	{
		[Key]
		public int IssueID { get; set; }
		public string Summary { get; set; }

		public virtual ICollection<Worklog> Worklogs { get; set; }
	}
}
