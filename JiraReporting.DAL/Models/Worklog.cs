﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JiraReporting.DAL.Models
{
	public class Worklog
	{
		[Key]
		public int WorklogID { get; set; }
		public string Author { get; set; }

		public DateTime Started { get; set; }
		public DateTime Updated { get; set; }

		public int TimeSpentSeconds { get; set; }

		public int IssueID { get; set; }
		public virtual Issue Issue { get; set; }
	}
}
